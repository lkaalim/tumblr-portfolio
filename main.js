
//Code by me, Luke!


//Once the window is finished loading all the assets, being script execution
window.onload = Initialize;



/*			<Globals>			*/

	var activeBackground;
	var Debug;
	var activeAudioManager

/*			<Initialize>			*/
	function Initialize()
	{
		Debug = new DebugMonitor();
		
		activeBackground = new AnimatedBackground();
		activeBackground.Initialize();
		
		activeAudioManager = new AudioManager([null,document.getElementById("podquestMusic"),document.getElementById("orcwardMusic")]);
		
		var volumeSwitch = new VolumeControl(document.getElementById("volumeSwitch"));
		
		var previousWorkScroller = new Scroller(document.getElementById("pastWorkPage"),document.getElementById("pastWorksScroll"));
		
		
		//Past work scrollers
		var podQuestScroller = new Scroller(document.getElementById("podquestScroller"),document.getElementById("podquestControls"));
		var orcwardScroller = new Scroller(document.getElementById("orcwardScroller"),document.getElementById("orcwardControls"));
		
		
		var pageViewer = new PageView();
	}

/*			<Audio Manager>				*/
	
	var AudioManager = function(tracks)
	{
		this.allTracks = tracks;
		this.currentTrack = this.allTracks[0];
		
		this.muted = true;
		
		var currentTransition = null;
	}
	
	AudioManager.prototype.Fade = function(element,fadeIn,callback)
	{
		if(element == null)
		{
			if(callback != null)
			{
				callback();
			}
			return;
		}
		
		var fadeIntervalId;
		
		console.log(fadeIn);
		console.log(element);
		console.log(callback);
		
		if(fadeIn)
		{
			fadeIntervalId = window.setInterval(function()
			{
				if(element.volume + 0.1 >= 1)
				{
					window.clearInterval(fadeIntervalId);
					console.log("finished fade");
					if(callback != null)
					{
						callback();
					}
				}
				else
				{
					element.volume += 0.1;
				}
			},100)
		}
		else
		{
			fadeIntervalId = window.setInterval(function()
			{
				if(element.volume - 0.1 <= 0)
				{
					window.clearInterval(fadeIntervalId);
					console.log("finished fade");
					if(callback != null)
					{
						callback();
					}
				}
				else
				{
					element.volume -= 0.1;
				}
			},100)
		}
		
		return fadeIntervalId;
	}
	
	AudioManager.prototype.SetMute = function(isMuted)
	{
		this.muted = isMuted;
		if(this.currentTrack == null)
		{
			return;
		}
		if(isMuted)
		{
			clearInterval(this.currentTransition);
			this.currentTransition = this.Fade(this.currentTrack,false,null);
		}
		else
		{
			this.currentTrack.currentTime = 0;
			this.currentTrack.play();
			clearInterval(this.currentTransition);
			this.currentTransition = this.Fade(this.currentTrack,true,null);
		}
	}
	
	AudioManager.prototype.SwitchTrack = function(newTrackIndex)
	{
		if(newTrackIndex >= this.allTracks.length)
		{
			return;
		}
		
		if(this.muted)
		{
			this.currentTrack = this.allTracks[newTrackIndex];
		}
		else
		{
			clearInterval(this.currentTransition);
			this.currentTransition = this.Fade(this.currentTrack,false,function(context)
			{
				return function()
				{
					clearInterval(context.currentTransition);
					context.currentTrack = context.allTracks[newTrackIndex];
					context.currentTrack.currentTime = 0;
					context.currentTrack.play();
					context.currentTransition = context.Fade(context.currentTrack,true,null);
				}
			}(this));
		}
	}

/*			<Scroller>				*/

	var Scroller = function(containerElement,controlsElement)
	{
		if(containerElement.className.includes("page"))
		{
			containerElement.addEventListener('onPageSwitch', function (scroller) {
				return function()
				{
					scroller.Refresh();
				}
			}(this), true);
		}
		
		this.container;
		
		for(var index = 0; index < containerElement.childNodes.length; index ++)
		{
			if(containerElement.childNodes[index].className == null)
			{
				continue;
			}
			
			if(containerElement.childNodes[index].className.includes("scrollerContainer"))
			{
				this.container = containerElement.childNodes[index];
				break;
			}
		}
		
		this.horizontal = this.container.className.includes("horizontal");
		
		if(this.container == null)
		{
			console.log("No Containter");
			return null;
		}
		
		this.allScrollableElements = [];
		
		for(var index = 0; index < this.container.childNodes.length; index ++)
		{
			if(this.container.childNodes[index].className == null)
			{
				continue;
			}
			
			if(this.container.childNodes[index].className.includes("scroll"))
			{
				if(!this.horizontal)
				{
					this.container.childNodes[index].style.display = "table";
				}
				this.allScrollableElements.push(this.container.childNodes[index]);
			}
		}
		
		if(!controlsElement.className.includes("scrollerControls"))
		{
			this.controls = controlsElement.getElementsByClassName("scrollerControls");
		}
		else
		{
			this.controls = controlsElement;
		}
		
		this.activeScrollIndex = 0;
		
		for(index = 0; index < this.controls.length; index++)
		{
			var currentControl = this.controls[index];
			var changeAmount = parseInt(currentControl.getAttribute("data-scrollerControls"));
			
			if(changeAmount + this.activeScrollIndex < 0 || changeAmount + this.activeScrollIndex >= this.allScrollableElements.length)
			{
				currentControl.className += " disabled";
			}
			
			currentControl.onclick = function(scroller,changeAmount)
			{
				return function()
				{
					scroller.activeScrollIndex += changeAmount;
					scroller.Refresh();
				}
			}(this,changeAmount)
		}
	}
	
	Scroller.prototype.Refresh = function()
	{
		var newPosition = 0;
		
		var horizontal = this.container.className.includes("horizontal");
		
		for(index = 0; index < this.activeScrollIndex; index++)
		{
			if(horizontal)
			{
				newPosition += this.allScrollableElements[index].offsetWidth;
			}
			else
			{
				newPosition += this.allScrollableElements[index].offsetHeight;
			}
		}
		if(horizontal)
		{
			this.container.style.transform = "translateX(" + -newPosition + "px)";
		}
		else
		{
			this.container.style.transform = "translateY(" + -newPosition + "px)";
		}
		
		for(index = 0; index < this.controls.length; index++)
		{
			var currentControl = this.controls[index];
			var changeAmount = parseInt(currentControl.getAttribute("data-scrollerControls"));
			
			if(changeAmount + this.activeScrollIndex < 0 || changeAmount + this.activeScrollIndex >= this.allScrollableElements.length)
			{
				if(!currentControl.className.includes("disabled"))
				{
					currentControl.className += " disabled";
				}
			}
			else
			{
				currentControl.className = currentControl.className.replace(" disabled","");
			}
			
		}
		
		var musicToPlay = this.allScrollableElements[this.activeScrollIndex].getAttribute("data-music");
		
		if(musicToPlay != null)
		{
			musicToPlay = parseInt(musicToPlay);
			activeAudioManager.SwitchTrack(musicToPlay);
		}
	}

	
/*			<PageView>				*/

	var PageView = function()
	{
		this.allPages = document.getElementsByClassName("page");
		this.currentPage = this.allPages[0];
		
		//console.log(this.currentPage.style.transition);
		
		var pageButtons = document.getElementsByClassName("pageButton");
		
		//inital parse through all the pages.
		for(index = 0; index < this.allPages.length; index++)
		{
			currentEntry = this.allPages[index];
			
			//disable all entries that arn't the active page.
			if(currentEntry != this.currentPage)
			{
				currentEntry.className += " disabled";
			}
			
			//add events to each one
			currentEntry.onPageSwitch = new Event('onPageSwitch');
		}
		
		//add the onlick functionality to all buttons
		for(index = 0; index < pageButtons.length; index++)
		{
			currentButton = pageButtons[index];
			//add the onclick function
			currentButton.onclick = function(pageView)
			{
				return function()
				{
					var pageIndex = this.getAttribute("data-pagelink");
					pageView.ChangePage(pageIndex);
				}
			}(this)
		}
	}
	
	PageView.prototype.ChangePage = function(newPageIndex)
	{
		this.currentPage = this.allPages[newPageIndex];
		
		this.currentPage.dispatchEvent(this.currentPage.onPageSwitch);
		
		this.currentPage.className = this.currentPage.className.replace(" disabled","");
		
		var musicToPlay = this.currentPage.getAttribute("data-music");
		
		if(musicToPlay != null)
		{
			musicToPlay = parseInt(musicToPlay);
			activeAudioManager.SwitchTrack(musicToPlay);
		}
		
		for(index = 0; index < this.allPages.length; index++)
		{
			currentEntry = this.allPages[index];
			
			if(currentEntry != this.currentPage && !currentEntry.className.includes(" disabled"))
			{
				currentEntry.className += " disabled";
			}
		}
	}

	
/*			<Volume Control>			*/
	
	var VolumeControl = function(sourceElement,audioManager)
	{
		this.sourceElement = sourceElement;
		this.manager = audioManager;
		this.volumeActive = false;
		
		this.switchWidth = sourceElement.clientWidth / 3;
		
		this.sourceElement.onclick = function(element)
		{
			return function ()
			{
				element.Toggle();
			}
		}(this)
	}
	
	VolumeControl.prototype.Toggle = function()
	{
		if(this.volumeActive)
		{
			this.volumeActive = false;
			console.log("Volume is muted");
			this.sourceElement.style.left = "0px";
			
		}
		else
		{
			this.volumeActive = true;
			console.log("Volume is unmuted");
			this.sourceElement.style.left = -this.switchWidth + "px";
		}
		
		activeAudioManager.SetMute(!this.volumeActive);
	}
	
	

/*			<Animated Background>			*/

	var AnimatedBackground = function()
	{
		//Animated Background memebers
		
		this.lastFrameTime = 0;
		
		this.deltaTimeDebug = Debug.MakeDebugElement();
		this.fpsDebug = Debug.MakeDebugElement();
		
		this.framesDrawn = 0;
		this.startTime = null;
		
		this.scene;
		this.camera;
		
		this.cube;
		
		this.webGLRenderer;
	}
	
	AnimatedBackground.prototype.ResizeBackground = function(background)
	{
		background.camera.aspect = window.innerWidth / window.innerHeight;
		background.camera.updateProjectionMatrix();
		background.webGLRenderer.setSize( window.innerWidth, window.innerHeight );
	}

	//Render Function, called 60 times per second. Theoretically.
	AnimatedBackground.prototype.Render = function(renderer)
	{
		//console.log("Actual Rendering function. This is who I think the renderer is:");
		//console.log(renderer);
		
		
		this.framesDrawn++;
		
		var currentTime = performance.now();
		
		//TimeDelta is the amount of seconds since the last frame was drawn.
		var timeDelta = (currentTime - this.lastFrameTime) / 1000;
		
		this.lastFrameTime = currentTime;
		
		var timeElapsed = currentTime - this.startTime;
		
		this.deltaTimeDebug.innerHTML = "delta time = " + timeDelta;
		this.fpsDebug.innerHTML = "fps: " + 1/timeDelta;
		
		this.webGLRenderer.render( this.scene, this.camera );
		//this.cube.rotation.x += 0.05 * timeDelta;
		this.cube.rotation.y += 0.05 * timeDelta;
		
		requestAnimationFrame
		(
			function()
			{
				//console.log("Subsequent animation requests: this is who I think the renderer is:");
				//console.log(renderer);
				renderer.Render(renderer);
			}
		);
	}

	AnimatedBackground.prototype.Initialize = function()
	{
		this.scene = new THREE.Scene();
		this.camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );


		this.webGLRenderer = new THREE.WebGLRenderer();

		this.webGLRenderer.setSize( window.innerWidth, window.innerHeight );
		
		this.webGLRenderer.domElement.id = "background_canvas";
		
		this.webGLRenderer.setClearColor(new THREE.Color(0x272842, 1.0));
		
		document.body.appendChild( this.webGLRenderer.domElement );
		
		window.onresize = function (background)
			{
				return function()
				{
					//console.log("newFrameFunction. This is who I think the renderer is:");
					//console.log(renderer);
					background.ResizeBackground(background);
				}
			}(this)
		
		
		var geometry = new THREE.SphereGeometry( 5, 32, 32);
		var material = new THREE.MeshBasicMaterial( { color: 0x5B5B5B } );
		
		material.wireframe = true;
		
		this.cube = new THREE.Mesh( geometry, material );
		this.scene.add( this.cube );

		this.camera.position.z = 2;
		
		this.startTime = performance.now();
		
		
		
		requestAnimationFrame
		(
			function (renderer)
			{
				return function()
				{
					//console.log("newFrameFunction. This is who I think the renderer is:");
					//console.log(renderer);
					renderer.Render(renderer);
				}
			}(this)
			
		);
		//console.log("Added callback.");

	}



/*			<Debug Monitor>			*/

	function DebugMonitor()
	{
		var debugElementParent = document.createElement("span");
		debugElementParent.className = "debugParent";
		document.body.appendChild(debugElementParent);
		
		this.parentElement = debugElementParent;
	}

	DebugMonitor.prototype.MakeDebugElement = function()
	{
		var debugElement = document.createElement("P");
		
		debugElement.className = "debug";
		
		this.parentElement.appendChild(debugElement);
		
		return debugElement;
	}